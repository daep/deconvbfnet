#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Training script

Flags are as definied in function run.

"""

import os
import random
import argparse
import yaml
import shutil
import warnings
import timeit

import numpy as np
import torch
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint

import deconvnetlib
deconvnetlib.check_version()


def set_seed(seed):
    """ Set random number set for multiple environemnts """
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    pl.seed_everything(seed)


def run(
    parameters=None,
    experiment='test',experiment_folder=None,
    database_folder=None,database_format=None,datapoints=0,
    epochs=100,deterministic=False,
    workers=4,batch_size=500,period=None,
    learning_rate=1e-3,patience=5,
    ):
    """ Set the neural network and perfoms the training
    
    ARGUMENTS
    ----------

        parameters: neural network parameters. It can be either a dictionary
            with the network parameters or a string with the path to a
            *.yml file containing the dict. Default value (None) uses the 
            template file present in the root folder.

        experiment: string with name of the experiment. To be used in name of
            generated folders and files. Optional, default is `test`.
            Warning: pre-existing files will be replaced.

        experiment_folder: string with the path where to record the experiment. 
            Optional, default (None) will consider subfolder `experiment` of
            current working folder.

        database_folder: string with the path where to read the database. 
            Optional, default (None) will consider subfolder `database` of
            current working folder.
        
        database_format: string with the format of the files with the database.
            Name of the file may include information such as the frequency.
            Optional, default (None) considers default filenames.

        datapoints: number of datapoints to consider during training. If this
            value is smaller than the number of datapoints available, a random
            subset is selected. Number of datapoints used on validation are
            fixed to 20% of those used in training. Optional, default (0), uses
            all the number of datapoints available

        epochs: number of epochs. Optional, default is 100.

        deterministic: boolean to define is run is determinisitic. Beta
            feature, not available for all functions. Optional, default
            is False.

        workers: number of CPU processes used for loading the database.
            Optional, default is 4.

        batch_size: batch size (number of input-output pairs considered 
            simultaneously). Optional, default is 500.
        
        period: interval (number of epochs) between checkpoints. Optional, if
            not given, 1/10 of the maximun number of epochs is used

        learning_rate: initial training learning rate. Optional, default value
            is 0.001

        patience: maximun number of epochs without improvement of validation
            loss before early stop. Optional, default is 5.

    RETURNS
    ----------

        model: trained neural network model on evaluation mode

        trainer: pytorch-lightining trainer instance. Contains, for instance,
           the losses and number of final epoch.

        elapsed_time: training clock time in seconds

    """
    
    library_folder = os.path.dirname(os.path.realpath(__file__))

    # doing calculation in GPU if available
    gpus = 0
    if torch.cuda.is_available(): gpus += 1

    if experiment is None:
        experiment = 'test'

    # by default, consider the experiment folder as the directory
    # where the script was called
    if experiment_folder is None:
        experiment_folder = os.getcwd() + '/' + experiment
    os.makedirs(f'{experiment_folder}', exist_ok=True)

    # by default, consider the sample parameters file
    if not isinstance(parameters, dict):
        if parameters is None:
            parameters_file = library_folder + '/parameters.yaml'
        else:
            parameters_file = parameters

        # copy file to experiment folder
        try:
            shutil.copy(
                parameters_file,
                f'{experiment_folder}/parameters.yaml'
                )
        except shutil.SameFileError:
            pass

        with open(parameters_file) as file:
            parameters = yaml.load(file, Loader=yaml.FullLoader)
    else:
        # creating a parameters file in the experiment's folder
        with open(f'{experiment_folder}/parameters.yaml', 'w') as file:
            yaml.dump(parameters, file, default_flow_style=False)
        

    if database_folder is None:
        database_folder = os.getcwd() + '/database/'

    if deterministic:
        set_seed(1)
        if gpus > 0:
            warnings.warn(
                'Current pytorch deterministic implementations on GPU\'s are '
                'only available for ZeroPad2d. Thus, the determinisitic flag '
                'just sets the random number seeds. Truly determinitic '
                'trainings are only possible with CPU\'s')
        else:
            print('Setting determinisitc flags...')
            torch.use_deterministic_algorithms(True)
            torch.backends.cudnn.benchmark = False
            torch.backends.cudnn.deterministic = True
    
    figs_folder = f'{experiment_folder}/figs/'
    results_folder = f'{experiment_folder}/results/'
    ckpts_folder = f'{experiment_folder}/ckpts/'
    for folder in [experiment_folder,figs_folder,results_folder,ckpts_folder]:
        os.makedirs(folder, exist_ok=True)

    loader_dict = dict(num_workers=workers,batch_size=batch_size)

    if database_format is None: database_format = r'{:}.npy'

    datasets, loaders = {}, {}
    # calculating the number of datapoints
    len_datasets = [datapoints,int(0.2*datapoints)]
    print('\nDefining dataset...')
    for step_name, step_len in zip(('training','validation'),len_datasets):
        datasets[step_name] = deconvnetlib.dataset.DeconvNetDataset(
            f'{database_folder:}/' + database_format.format(step_name) )
        
        # random sub-sampling if number of datapoints is limited
        if step_len > 0 and step_len < len(datasets[step_name]):
            subset_indice = torch.randint(
                low=0,high=len(datasets[step_name]),size=[step_len])
            datasets[step_name] = torch.utils.data.Subset(
                dataset=datasets[step_name],
                indices=subset_indice,
                )
        print(f'{step_name:>15}: {len(datasets[step_name]):>8d}')
        
        loaders[step_name] = torch.utils.data.DataLoader(
            datasets[step_name],**loader_dict,shuffle=step_name=='training')
    print('')

    net = deconvnetlib.network.DeconvNet(**parameters)
    # displaying architecture on terminal
    net.summary()

    # explicitly initiating the weights
    for layer in net.children():
        try:
            torch.nn.init.kaiming_uniform_(layer.weight)
        except(AttributeError):
            pass # padding has no initialization
    
    if period is None: period = epochs // 10
    
    checkpoint_callback = ModelCheckpoint(
        dirpath=ckpts_folder,
        filename=experiment + '_{epoch:06d}_{val_loss:6.2e}',
        verbose=True,
        monitor='val_loss',
        mode='min',
        period=period,
        save_last=True,
    )

    early_stop_callback = pl.callbacks.early_stopping.EarlyStopping(
        monitor='val_loss',
        min_delta=0.0, patience=patience,
        verbose=False, mode='min',strict=True
        )

    trainer = pl.Trainer(
        max_epochs=epochs,
        callbacks=[
            deconvnetlib.callbacks.PrintCallback(figs_folder),
            early_stop_callback],
        checkpoint_callback=checkpoint_callback,
        gpus=gpus,
        progress_bar_refresh_rate=0,
        logger=None,
        deterministic=deterministic,
        )
    
    hparams = dict(
        experiment_folder=experiment_folder,
        results_folder=results_folder,
        learning_rate=learning_rate,
    )
    model = deconvnetlib.network.DeconvNetModule(net,hparams)

    start = timeit.default_timer()
    trainer.fit(
        model,
        train_dataloader=loaders['training'],
        val_dataloaders=loaders['validation'],
        )
    end = timeit.default_timer()
    
    # copying the best model checkpoint file
    try: # in the case of no best has been defined
        shutil.copy(
            checkpoint_callback.best_model_path,
            ckpts_folder + '/best.ckpt'
        )
    except(FileNotFoundError):
        shutil.copy(
            ckpts_folder + '/last.ckpt',
            ckpts_folder + '/best.ckpt'
        )

    elapsed_time = end - start
    print(f'Elapsed time (in seconds): {elapsed_time:12.3f}')
    print('#'*72)

    model.eval()

    return model, trainer, elapsed_time


if __name__ == '__main__':
    # doing a standard run, using arguments as input
    parser = argparse.ArgumentParser()
    parser.add_argument('-parameters',default=None)
    parser.add_argument('-deterministic',action='store_true')
    parser.add_argument('-experiment',default=None)
    parser.add_argument('-experiment_folder',default=None)
    parser.add_argument('-database_folder',default=None)
    parser.add_argument('-database_format',default=None)
    parser.add_argument('-datapoints',default=0,type=int)
    parser.add_argument('-epochs',default=100,type=int)
    parser.add_argument('-workers',default=4,type=int)
    parser.add_argument('-batch_size',default=500,type=int)

    args = vars(parser.parse_args())

    run(**args)