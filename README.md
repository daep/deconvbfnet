# DeconvBFNet

Convolutional neural network used to deconvolute beamforming maps, as presented in:

> W Gonçalves Pinto, M Bauerheim, and H Parisot-Dupuis. "Deconvoluting acoustic beamforming maps with a deep neural network". *50th International Congress and Exposition on Noise Control Engineering*, August 2021.

If you find this code useful in your research, please consider citing:

```
@article{Goncalves2021,
    title = "Deconvoluting acoustic beamforming maps with a deep neural network",
    journal = "INTER-NOISE and NOISE-CON Congress and Conference Proceedings",
    year = "2021",
    volume = "263",
    number = "1",
    pages = "5397-5408",
    issn = "0736-2935",
    url = "https://www.ingentaconnect.com/content/ince/incecp/2021/00000263/00000001/art00057",
    doi = "doi:10.3397/IN-2021-3084",
}
```

This work is a part of the POLA3 project.

<table border="1" align="center">
  <thead>
    <tr>
      <th style="background-color:#82afbd" colspan="6" align="center">Example of deconvoluted map obtained with classical methods and with the convolutional neural network (CNN)</th>
    </tr>
  </thead>
  <tbody>
    <tr style="background-color:#a6e4f7">
      <td align="center">frequency</td>
      <td align="center"><b>CBF</b></td>
      <td align="center"><b>DAMAS</b></td>
      <td align="center"><b>CLEAN-SC</b></td>
      <td align="center"><b>CNN</b></td>
      <td align="center"></td>
    </tr>
    <tr>
      <td align="center">1 kHz</td>
      <td align="center"><img alt="Conventional beamforming map, 1 kHz" src="./images/cropped_map_f01000Hz_CBF.png" height="180"/></td>
      <td align="center"><img alt="DAMAS map, 1 kHz" src="./images/cropped_map_f01000Hz_DAMAS.png" height="180"/></td>
      <td align="center"><img alt="CLEAN-SC map, 1 kHz" src="./images/cropped_map_f01000Hz_CLEAN-SC.png" height="180"/></td>
      <td align="center"><img alt="CNN map, 1 kHz" src="./images/cropped_map_f01000Hz_CNN.png" height="180"/></td>
      <td align="center"><img alt="colorbar, 1 kHz" src="./images/colorbar_f01000Hz.png" height="180"/></td>
    </tr>
    <tr>
      <td align="center">4 kHz</td>
      <td align="center"><img alt="Conventional beamforming map, 4 kHz" src="./images/cropped_map_f04000Hz_CBF.png" height="180"/></td>
      <td align="center"><img alt="DAMAS map, 4 kHz" src="./images/cropped_map_f04000Hz_DAMAS.png" height="180"/></td>
      <td align="center"><img alt="CLEAN-SC map, 4 kHz" src="./images/cropped_map_f04000Hz_CLEAN-SC.png" height="180"/></td>
      <td align="center"><img alt="CNN map, 4 kHz" src="./images/cropped_map_f04000Hz_CNN.png" height="180"/></td>
      <td align="center"><img alt="colorbar, 4 kHz" src="./images/colorbar_f04000Hz.png" height="180"/></td>
    </tr>
    <tr>
      <td align="center">16 kHz</td>
      <td align="center"><img alt="Conventional beamforming map, 16 kHz" src="./images/cropped_map_f16000Hz_CBF.png" height="180"/></td>
      <td align="center"><img alt="DAMAS map, 16 kHz" src="./images/cropped_map_f16000Hz_DAMAS.png" height="180"/></td>
      <td align="center"><img alt="CLEAN-SC map, 16 kHz" src="./images/cropped_map_f16000Hz_CLEAN-SC.png" height="180"/></td>
      <td align="center"><img alt="CNN map, 16 kHz" src="./images/cropped_map_f16000Hz_CNN.png" height="180"/></td>
      <td align="center"><img alt="colorbar, 16 kHz" src="./images/colorbar_f16000Hz.png" height="180"/></td>
    </tr>
  </tbody>
</table>


## Installing dependencies

Neural network is implemented using frameworks [PyTorch](https://pytorch.org/) (version 1.8.0) and [PyTorch Lightning](https://www.pytorchlightning.ai/) (1.2.4). File `conda_env.yml` contains the list and versions of the packages used by the library.

To install all dependencies, one may use [conda](https://anaconda.org/):

```shell
conda env create -f ./dependencies/conda_env.yml --verbose
```

Acoustic beamforming package [Beamlib](https://gitlab.isae-supaero.fr/acoustic-beamforming/beamlib) is not required for training a model but necessary if one wants to use the implemented database generation and testing funcionalities. To install it you can either clone the repository or install the package's wheel available in the [dependencies](/depdendencies) folder by running (preferably after activating the dedicated conda environment):

```shell
pip install dependencies/Beamlib-0.20-py3-none-any.whl
```

## Usage

Folder [deconvnetlib](deconvnetlib) contains the DeconvNet implementation and scripts associated with the database generation and loading. Neural network architecture is defined in [deconvnetlib/network.py](deconvnetlib/network.py).
Scripts `generate.py`, `train.py` and `test.py` provide interfaces to performing database generation, neural network training and testing from a shell script. Example of calls and a description of the flags are presented next. More elaborate scripts can be found in the [templates](templates) folder.

For a sample training, you can run (from this folder):

```shell
bash templates/install.sh
mkdir __cache__
cd __cache__
bash ../templates/minimal.sh
```

Script `install.sh` creates a conda env and install Beamlib while `minimal.sh` runs all steps of a small training, from database generation to testing.

### Generating the database

Example of shell script used to generate 3 databases (training, validation and testing). On each case, 1 source is randomly placed on the scannning grid (21 x 21). The [Beamlib](https://gitlab.isae-supaero.fr/acoustic-beamforming/beamlib) package must be installed, `LIB_FOLDER` indicates the path to this library and should be adapted for each machine.

```shell
#!/bin/bash
. "${HOME}/anaconda3/etc/profile.d/conda.sh"
conda activate deconvbfnet

LIB_FOLDER="${HOME}/dev/deeplearning/deconvbfnet"

python "${LIB_FOLDER}/generate.py" -n=1000 -min=1 -max=1 -workers=9 -filename=training
python "${LIB_FOLDER}/generate.py" -n=500 -min=1 -max=1 -workers=9 -filename=validation
python "${LIB_FOLDER}/generate.py" -n=100 -min=1 -max=1 -workers=9 -filename=testing
```

| flag | default | description |
|-|-|-|
| `n` | `1000` | number of cases. |
| `min` | `1` | minimun number of sources. |
| `max` | `10` | maximun number of sources. |
| `range` | `0` | amplitudes range in decibels relative to the stronges source. All sources have the same amplitude when range is null (default). |
| `benchmarks` | `False` | flag to indicate to generate the signals for benchmark cases, as defined in deconvnetlib.benchmarks. If used, `min`, `max` and `range` flags are ignored. |
| `workers` | `5` | number of processes called to generate the databse. |
| `datafolder` | `None` | string with the path where to save the database. By default will consider subfolder `database` of current working folder. |
| `filename` | `None` | string with the filename to be generated (without the file format). By default, a file with the frequency and number of cases will be used. |
| `frequency` | `2000` | beamforming frequency in Hertz. |
| `size` | `21` | scanning grid discretization. By default will generate datafor a 21 x 21 grid. |


### Training

Example of shell script used to train a model with default parameters (folder, database, etc), for 50 epochs:

```shell
#!/bin/bash
. "${HOME}/anaconda3/etc/profile.d/conda.sh"
conda activate deconvbfnet

LIB_FOLDER="${HOME}/dev/deeplearning/deconvbfnet"

CUDA_VISIBLE_DEVICES=1 python "${LIB_FOLDER}/train.py" -epochs=50
```

| flag | default | description |
|-|-|-|
| `parameters` | `None` | neural network parameters. It can be either a dictionary with the network parameters or a string with the path to a `*.yml` file containing the dict. Default value uses the template file present in the root folder. |
| `deterministic` | `False` | if activated, do a determinisitic run. Beta feature, not available for all functions |
| `experiment` | `test` | string with name of the experiment. To be used in name of generated folders and files. Warning: pre-existing files will be replaced. |
| `experiment_folder` | `None` | string with the path where to record the experiment. Default will consider subfolder `experiment` of current working folder.|
| `database_folder` | `None` | string with the path where to read the database. Default will consider subfolder `database` of current working folder. |
| `database_format` | `None` | string with the format of the files with the database. Name of the file may include information such as the frequency. Optional, default (None) considers default filenames. |
| `datapoints` | `0` | number of datapoints to consider during training. If this value is smaller than the number of datapoints available, a random subset is selected. Number of datapoints used on validation are fixed to 20% of those used in training. Default uses all the number of datapoints available |
| `epochs` | `100` | number of epochs
| `workers` | `4` | number of CPU processes used for loading the database. |
| `batch_size` | `500` | batch size (number of input-output pairs |

### Testing

Example of shell script used to test the trained model and to compare it to classical deconvolution algorithms. The trained model (checkpoint) will be loaded from `./experiments/test/ckpt`, and the inference will be performanced for every data point in database file `./database/testing_f02000Hz.npz` (default database folder is `./database`) and calculate the comparison criteria. The [Beamlib](https://gitlab.isae-supaero.fr/acoustic-beamforming/beamlib) package must be installed.

```shell
#!/bin/bash
. "${HOME}/anaconda3/etc/profile.d/conda.sh"
conda activate deconvbfnet

LIB_FOLDER="${HOME}/dev/deeplearning/deconvbfnet"
case='test'
# force it to be done in CPU, even if GPU is available
CUDA_VISIBLE_DEVICES= \
    python "${LIB_FOLDER}/test.py" \
    -parameters="./experiments/${case}/parameters.yaml" \
    -case_name="testing" \
    -database_format='{:}_f02000Hz.npz' \
    -experiment_folder="./experiments/${case}" \
    2>&1 | tee "experiments/${case}/log.testing_${case}"
```

| flag | default | description |
|-|-|-|
| `parameters` | `None` | neural network parameters. It can be either a dictionary with the network parameters or a string with the path to a `*.yml` file containing the dict. Default value uses the template file present in the root folder. |
| `experiment_folder` | `None` | string with the path where to record the experiment. Default will consider subfolder `experiment` of current working folder.|
| `database_folder` | `None` | string with the path where to read the database. Default will consider subfolder `database` of current working folder. |
| `database_format` | `None` | string with the format of the files with the database. Name of the file may include information such as the frequency. Optional, default (None) considers default filenames. |
| `case_name` | `None` | string with the name of the case to be used as is replaced in the `database_format` attribute |
| `array` | `None` | filename with the array. By default reads the file `array.xml` in the database folder. |
| `grid` | `None` | filename with the scanning grid. By default reads the file `grid.xml` in the database folder. |
| `workers` | `10` | number of subprocesses used for comparing the cases. |
