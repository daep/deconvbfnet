#!/bin/bash
#
# Sample shell file for training a model
#

. "${HOME}/anaconda3/etc/profile.d/conda.sh"
conda activate deconvnet

LIB_FOLDER="${HOME}/dev/deeplearning/deconvnet"

database_name='Underbrink65_z120cm_Incoherent_1to10_21x21'
workers=20

case='test'
frequency=4000
range=0

frequency=$(printf "%05d" $frequency)

CUDA_VISIBLE_DEVICES=0 \
    python "${LIB_FOLDER}/train.py" \
        -experiment=${case} \
        -experiment_folder="./experiments/${case}" \
        -epochs=250 \
        -database_folder="../database/${database_name}/" \
        -database_format="{:}_${range}dB_f${frequency}Hz.npz" \
        -deterministic \
        2>&1 | tee "experiments/${case}/log.training_${case}"
