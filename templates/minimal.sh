#!/bin/bash

# Script with an example of a complete training procedure, from
# database generation to testing. On a quad-core notebook it
# should take about 7 minutes to run all commands. Generate around
# 90 Mb of data (database + test files).

# Adapt your conda env name and deconvnet repository paths if necessary

# starting conda and the env
. "${HOME}/anaconda3/etc/profile.d/conda.sh"
LIB_FOLDER="${HOME}/dev/deeplearning/deconvbfnet"
conda activate deconvnet

date
echo "=========="

echo "Generating the databases (2000 Hz, 1 to 5 incoherent sources)..."
sizes=(1000 200 10)
names=('training' 'validation' 'testing' )
for i in "${!names[@]}"
do
    echo ${names[i]}
    python "${LIB_FOLDER}/generate.py" \
        -n=${sizes[i]} -max=5 -workers=4 -filename=${names[i]}
done
echo "=========="

echo "Launching the training..."
python "${LIB_FOLDER}/train.py" \
    -epochs=100 \
    -database_format='{:}_f02000Hz.npz'
echo "=========="

echo "Testing the trained model..."
python "${LIB_FOLDER}/test.py" \
    -experiment_folder="test" \
    -case_name="testing" \
    -database_format='{:}_f02000Hz.npz' \
    -workers=4
echo "=========="

date
