#!/bin/bash
#
# Sample shell file for creating a multi-frequency database
#

. "${HOME}/anaconda3/etc/profile.d/conda.sh"
conda activate deconvnet

LIB_FOLDER="${HOME}/dev/deeplearning/deconvnet"

database_name='Underbrink65_z120cm_Incoherent_1to10_21x21'
workers=20

# loop for several frequencies (octave bands central frequency)
for frequency in 1000 2000 4000 8000 16000
do
    # with non constant and constant source amplitudes
    for range in 10 0
    do
        # validation database
        python "${LIB_FOLDER}/generate.py" \
            -n=1000 -min=1 -max=10 -workers=$workers \
            -range=${range} -frequency=${frequency} \
            -datafolder="./${database_name}/" \
            -filename="validation_${range}dB"
        # training database
        python "${LIB_FOLDER}/generate.py" \
            -n=5000 -min=1 -max=10 -workers=$workers \
            -range=${range} -frequency=${frequency} \
            -datafolder="./${database_name}/" \
            -filename="training_${range}dB"
        # testing database
        python "${LIB_FOLDER}/generate.py" \
            -n=100 -min=1 -max=10 -workers=$workers \
            -range=${range} -frequency=${frequency} \
            -datafolder="./${database_name}/" \
            -filename="testing_${range}dB"
    done
    
    # generating the benchmarks cases
    python "${LIB_FOLDER}/generate.py" \
        -workers=$workers \
        -frequency=${frequency} \
        -datafolder="./${database_name}/" \
        -benchmarks

done
