#!/bin/bash

# Script with the installation of the dependencies using conda.
# Adapt your conda and deconvnet repository paths if necessary

. "${HOME}/anaconda3/etc/profile.d/conda.sh"
LIB_FOLDER="${HOME}/dev/deeplearning/deconvbfnet"

# this will create a deconvnet env on conda default folder
# you may add flag `--prefix <desired path>` to change it
conda env create -f "${LIB_FOLDER}/dependencies/conda_env.yml" --verbose

# installing beamlib
conda activate deconvbfnet
pip install "${LIB_FOLDER}/dependencies/Beamlib-0.20-py3-none-any.whl" 
