#!/bin/bash
# Script to crop the images

# separating the colorbars
for f in 01000 04000 16000
do
    convert \
	"maps_predefined_N007_002_f${f}Hz_CBF_withSources.png" \
        -crop 200x815+720+112 \
        "colorbar_f${f}Hz.png"
done

# cropping the images
for filename in maps_*.png
do
    # split filename by underscores
    filename_arr=(${filename//_/ })
    convert \
        ${filename} \
	-crop 695x815+32+112 \
	"cropped_map_${filename_arr[-3]}_${filename_arr[-2]}.png"
done
