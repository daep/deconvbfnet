#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Testing script

----------
Flags:
    
    -parameters: path to the file containing the model paramerters.
        By default (None), considers the `parameters.yaml` available in
        the experiments folder.
    
    -experiment_folder: path to the experiment. By default (None), considers
        the experiment folder as the directory where the script was called
    
    -database_folder: string with the path where to read the database. 
        Optional, default (None) will consider subfolder `database` of
        current working folder.
    
    -database_format: string with the format of the files with the database.
        Name of the file may include information such as the frequency.
        Optional, default (None) considers default file format.
    
    -case_name: string with the name of the case to be used as is
        replaced in the `database_format` attribute
    
    -array: filename with the array. Default (None) reads the file
        `array.xml` in the database folder.
    
    -grid: filename with the scanning grid. Default (None) reads the file
        `grid.xml` in the database folder.
    
    -workers: number of subprocesses used for comparing the cases
    
----------
Example:
    
    To test a model stored in folder './test', for a database file named
    './database/random_2kHz.npz', using default parameters, array and grid
    files:
        
        ```
        python test.py \
            -experiment_folder='./test' \
            -case_name='random' \
            -database_format='{:}_2kHz.npz'
        ```

"""

import argparse

import deconvnetlib
deconvnetlib.check_version()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-parameters',default=None)
    parser.add_argument('-experiment_folder',default=None)
    parser.add_argument('-database_folder',default=None)
    parser.add_argument('-database_format',default=None)
    parser.add_argument('-case_name',default=None)
    parser.add_argument('-array',default=None)
    parser.add_argument('-grid',default=None)
    parser.add_argument('-workers',default=10,type=int)

    args = parser.parse_args()

    database_folder = args.database_folder
    if database_folder is None: database_folder = './database/'
    database_format = args.database_format
    if database_format is None: database_format = '{:}.npz'
    case_name = args.case_name
    if case_name is None: case_name = 'testing'

    tester = deconvnetlib.tester.Tester(
        args.parameters,args.experiment_folder,database_folder,database_format)
    
    # testing - comparison of target and estimation pairs
    tester.test(case_name)
    
    workers = args.workers
    array = args.array
    grid = args.grid
    
    # comparing - evaluating CNN versus canonical deconvolution methods
    tester.compare(case_name,array=array,grid=grid,workers=workers)
    