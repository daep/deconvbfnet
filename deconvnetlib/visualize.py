#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Functions for plotting the results

"""

import numpy as np

import matplotlib
matplotlib.use('Agg') # backend only
import matplotlib.pyplot as plt
import matplotlib.ticker

import beamlib


FIGURE_SIZE=(8/2.54,6/2.54)
FIGURE_SIZE_LARGE=(16/2.54,5/2.54)


def plot_convergence(epochs,train_loss,val_loss):
    """ Creating a plot with the convergence
    """
    fig, ax = plt.subplots(figsize=FIGURE_SIZE,constrained_layout=True)
    
    ax.plot(epochs,train_loss,'-k',label='train')
    ax.plot(epochs,val_loss,':r',label='valid')
    
    ax.set_yscale('log')
    
    ax.set_xlabel('epochs', fontsize=7)
    ax.set_ylabel('loss', fontsize=7)
    ax.tick_params(axis='both', which='major', labelsize=6)
    ax.tick_params(axis='both', which='minor', labelsize=5)
    
    ax.legend(fontsize=6)
    ax.grid(True, which='both', linewidth=.5, alpha=.5, color='gray')
    
    return fig, ax


def plot_grid(X, Y, A, ax=None, cmap='hot_r'):
    """ Plot array A as a rectangular grid
    
    A wrapper to Beamlib's homonymous function.
    
    ARGUMENTS
    ----------

        X, Y: mesh grids indicating the x and y coordinates of the points
        
        A: 2D array to be plotted
        
        ax: axis where to plot the array. Optional, if None (default), a 
            new figure is created

        cmap: colormap. Default is reverse hot colormap (`hot_r`), with white
            at the beginning and black at the end

    RETURNS
    ----------
        
        fig: figure that is parent of the axis. If axis is given as input,
            returns the current figure

        ax: axis with the plotted image. If axis is given as input, returns
            the same

        mappable: plotted image

    """
    
    grid_x, grid_y = X[0,:], Y[:,0]
    fig, ax, mappable = beamlib.visualize.surface.plot_grid(
        x=grid_x, y=grid_y, Z=A, ax=ax, cmap=cmap, rasterized=False)
    
    return fig, ax, mappable


def add_colorbar(fig,mappable,ax=None,label=None,**kwargs):
    """ Add colorbar to a figure for a given mappable
    
    ARGUMENTS
    ----------

        fig: figure where to create the colorbar
        mappable: mappable associated to the colorbar
        ax: axis to create the colorbar. Optional, default is None (consider
            the current axis)
        label: string with the label to be added as the colorbar title
        **kwargs: colorbar args
    
     """

    if ax is None:
        ax = plt.gca()

    cbar = fig.colorbar(mappable, ax=ax, **kwargs,)

    cbar.ax.tick_params(labelsize=6)
    if label is not None:
        cbar.set_label(label, size=7)

    return cbar


def plot_evaluation(x,y,y_hat,filename):
    """ Creating a plot with input, target and output maps
    
    ARGUMENTS
    ----------
    
        x: tensor with a batch of inputs, format (batch,1,nx,ny)
        y: tensor with a batch of target, format (batch,1,nx,ny)
        y_hat: tensor with a batch of estimations, format (batch,1,nx,ny)
        filename: string with file format indicating the filename, must
            include a place to indicate the number of the test pair
    
    """
    
    # using a single figure to avoid memory overload
    fig, axs = plt.subplots(
        nrows=1,ncols=3,
        figsize=FIGURE_SIZE_LARGE,
        constrained_layout=True
        )

    scan_grid_n = x.shape[-1]
    
    # sending to the CPU and reshaping to 2D arrays
    prepare_to_plot = lambda a: a.detach().cpu().numpy().reshape(
        (scan_grid_n,scan_grid_n))
    
    labels = ('source map','beamforming map','deconvoluted map')
    
    index = 0
    number_cases = x.shape[0]
    # loop for all the cases in batch
    for inp, target, out in zip(x,y,y_hat):
        data = [
            prepare_to_plot(target),
            prepare_to_plot(inp),
            prepare_to_plot(out)
            ]
        if index == 0:
            scan_x = np.linspace(-0.5,0.5,num=scan_grid_n)
            scan_y = scan_x.copy()
            scan_X, scan_Y = np.meshgrid(scan_x,scan_y)
            
            images, cbars = [], []
            for arr, ax, label in zip(data,axs,labels):
                _, _, image = plot_grid(scan_X,scan_Y,arr,ax=ax,)
                images.append(image)
                cbars.append(
                    add_colorbar(
                        fig,image,ax=ax,
                        shrink=0.5,pad=0.0,fraction=0.10,aspect=30)
                )
                ax.xaxis.set_label_position('top')
                ax.set_xlabel(label, fontsize=7)
            
            axs[1].set_yticks([])
            axs[2].set_yticks([])

            # using scientific notation for beamforming map colorbar
            mf = matplotlib.ticker.ScalarFormatter(useMathText=True)
            mf.set_powerlimits((0, 0))
            cbars[1].ax.yaxis.set_major_formatter(mf)
            offset_text = cbars[1].ax.yaxis.get_offset_text()
            offset_text.set_size(6)
            offset_text.set_horizontalalignment('center')
            # set a different colormap to the beamforming map so there is no
            # confusion with the other two plots
            images[1].set_cmap('inferno_r')

            # force colorbar limits of deconvoluted map - in order to
            # make a direct comparison with the source map
            images[2].set_clim([0,1])
        else:
            for arr, image in zip(data, images):
                image.set_data(arr)

        # update colormap limits for beamforming map
        images[1].set_clim([np.min(data[1]),np.max(data[1])])
        cbars[1].ax.yaxis.set_major_formatter(mf)
        cbars[1].ax.yaxis.get_offset_text().set_horizontalalignment('center')

        if index % 10 == 0: print(f'{index:06d}/{number_cases:06d}')
        fig.savefig(filename.format(index),dpi=350,transparent=True)
        index += 1

    return fig, axs


def fancy_scientific(string):
    """ Convert value in scientific notation to mathtext """
    values = string.split('e')
    base = float(values[0])
    exponent = int(values[1])
    return f'${base:.2f}' + r' \times 10^{' + f'{exponent:d}' + '}$'


if __name__ == '__main__':
    pass
