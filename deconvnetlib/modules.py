#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Custom neural network blocks (modules)

"""

import numpy as np

import torch
from torch import nn
import torch.nn.functional as F


class TukeyPad2d(nn.Module):
    """ Tukey window padding 
    
    Applies a Tukey window for every dimension of the tensor. First, a 
    replication padding is applied followed by the product of the tensor by
    a kernel (same size of the channels).
    
    One can change amplitude of the product at the borders by combining the
    parameters `border` and `offset`
    
    ARGUMENTS
    ----------
    
        kernel_size: channel size
        padding: size of padding
        border: number of points used for the transition at the border
            limites 
        offset: applied push to the transiton outside the channel
        
    """
    def __init__(self,kernel_size,padding=1,border=3,offset=2):
        super().__init__()
        
        default_type = torch.tensor([1.0]).dtype

        self.pad = nn.ReplicationPad2d(padding=padding)
        
        kernel = 1
        mesh_grids = torch.meshgrid(
            [ torch.arange(size) for size in kernel_size ]
        )
        
        aN = border*2
        for axis, (size, grid) in enumerate(zip(kernel_size, mesh_grids)):
            window = np.ones((grid.shape[axis] + 2*offset,1))
            range_border = torch.arange(0,aN//2)
            window[range_border,0] = 1/2*(
                1 - torch.cos(2*np.pi*range_border/(aN)) )
            window[-(range_border+1),:] = window[range_border,:]
            window = torch.tensor(window[offset:-offset],dtype=default_type)

            if axis == 0:
                window = window.repeat(1,grid.shape[0],)
            else:
                window = window.T.repeat(grid.shape[1],1,)

            kernel *= window
        
        # reshape to depthwise convolutional weight
        kernel = kernel.view(1, 1, *kernel.size())
        kernel = kernel.repeat(1, *[1] * (kernel.dim() - 1))
        self.register_buffer('kernel', kernel)
    
    def forward(self, x):
        return self.pad(x) * self.kernel


class MaxNorm2d(nn.Module):
    """ Normalize channels by max value
    
    Similar to `InstanceNorm2d`, but considers the maximun and not the
    standard deviation for the normalization.
    
    ARGUMENTS
    ----------
    
        eps: a value added to the denominator to avoid NaNs
            Optional, default is 1e-20
    
    """
    def __init__(self,eps=1e-20):
        super().__init__()
        self.eps = eps
    
    def forward(self, x):
        ref = F.max_pool2d(x,kernel_size=x.shape[-2:])
        # avoid divide by 0
        ref += self.eps
        return x/ref

