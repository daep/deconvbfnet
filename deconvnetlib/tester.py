#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Classes and functions for evaluating the NN model and comparing it with
classical deconvolution methods.

It uses Beamlib's implementations of the deconvolution methods
Beamlib is available at:
    https://gitlab.isae-supaero.fr/acoustic-beamforming/beamlib

"""

import os
import glob
import functools
import multiprocessing
import multiprocessing.managers

import numpy as np
import torch
import matplotlib.pyplot as plt

import deconvnetlib
import beamlib


def check_glob(func):
    """ Decorator to check if given filename is a glob expression 
    
    In the case of a glob, loops the function call to all available files.
    
    TODO: make it more general and move it to tools
    
    """
    def wrapper(self,*args,**kwargs):
        
        # check if case_name is passed as kwarg
        if 'case_name' in kwargs:
            case_name = kwargs['case_name']
            del kwargs['case_name']
        else:
            case_name = args[0]
        
        base_name = self.database_format.replace(r'{:}','')
        expr = self.database_folder + '/' + \
               self.database_format.format(case_name)
        
        list_cases = glob.glob(expr)
        results = []
        for path in list_cases:
            case_name = os.path.basename(path)[:].replace(base_name,'')
            results.append(func(self,case_name,*args[1:],**kwargs))
        if len(results) == 1: results = results[0]
        return results
          
    return wrapper


class Tester():
    
    def __init__(self,parameters,experiment_folder=None,
             database_folder=None,database_format=None):
        """ Class used to test the trained model
        
        ----------
        ARGUMENTS
        
            parameters: dictionary or path to the file containing the neural
                netwotk model paramerters. By default (None), reads the
                `parameters.yaml` file available in the experiments folder.
        
            experiment_folder: path to the experiment. By default (None),
                 considers the experiment folder as the directory where the
                 script was called
        
            database_folder: string with the path where to save the database.
                Optional, default (None) will consider subfolder `database`
                of current working folder.
            
            database_format: string with the format of the files with
                the database. Optional, default (None) considers default file
                format.
        
        """
        
        if database_folder is None: database_folder = './database/'
        if database_format is None: database_format = '{:}.npz'
        
        self.model, self.parameters, self.folders = deconvnetlib.loader.load_model(
            experiment_folder,parameters,
            checkpoint_file=None,verbose=True)
        
        self.database_folder = database_folder
        self.database_format = database_format
        self.experiment_folder = self.folders['exp']
    
    
    @check_glob
    def test(self,case_name):
        """ Compares and plots target and estimated source maps
        
        Since it only considers the target/estimation pairs, array and grid
        informations are not necessary
        
        ARGUMENTS
        ----------
        
            case_name: string with the name of the case to be used as is
                replaced in the `database_format` attribute
        
        RETURNS
        ----------
        
            numpy array with the results
        
        """
        
        filename = self.database_folder + '/' + self.database_format.format(case_name)
        fig_fileformat = self.folders['figs'] + f'/test_{case_name}' + '_{:06d}'
        result_file = self.folders['results'] + f'/test_{case_name}.csv'
        
        losses, count_target, count_estimation, ratios = evaluate_model(
            self.model,filename,plot=True,filename_format=fig_fileformat)
        results = (losses, count_target, count_estimation, ratios)
        
        deconvnetlib.tools.export_csv(
            result_file,
            names=('loss','target','output','ratio'),
            data=results,
            )
            
        with open(result_file,'r') as f:
            print(f.read() + '\n')
        
        return np.array([res.detach().numpy() for res in results])
    
    
    @check_glob
    def compare(self,case_name,array=None,grid=None,
                frequency=2000,c0=343,workers=10,number_cases=0,**kwargs):
        """ Compares and plots maps obtained by deconvolutionm methods and NN
        
        Threshold used to define a source is the MSL (Maximun Sidelobe Level)
        returned by the `qualify` method for a source at the center of the
        scanning grid.

        ARGUMENTS
        ----------
        
            case_name: string with the name of the case to be used as is
                replaced in the `database_format` attribute
                
            array: microphone array for the analysis. It can be either a
                filename or an instance of the Array class. Optional, by
                default (None) reads the file `array.xml` in the database folder.
                
            grid: grid for the analysis. It can be either a filename or an
                instance of the Grid class. Optional, by default (None) 
                reads the file `grid.xml` in the database folder.
                
            frequency: beamforming frequency in Hertz. Optional, default is
                2000 Hz.
        
            c0: speed of sound in m/s. Optional, default is 343 m/s
                
            workers: number of subprocesses used for comparing the cases.
                Optional, default is 10.

            number_cases: number of cases to consider. Optional, if 0
                (default), all cases in dataset are considered.
            
            **kwargs: keword arguments to be passed to `_compare_methods`
                function
        
        RETURNS
        -------
        
            results: numpy array with the comparison criteria for the cases
                available on the database in shape [num_cases,4,4].
                More details in `_compare_methods`
        
        """
        
        # load the microphone array and the scanning grid
        if array is None: array = self.database_folder + '/array.xml'
        if type(array) == str: array = beamlib.arrays.ArrayBase(array)
        
        if grid is None: grid = self.database_folder + '/grid.xml'
        if type(grid) == str: grid = beamlib.grid.GridBase().load(grid)
        
        # calculating the Maximun Sidelobe Level (MSL) for a central source
        grid_refined = grid.refine(n=101)
        _, level_threshold = array.qualify(
            frequency=frequency,grid=grid_refined,c0=c0,max_level=-5)
        
        beamforming_args = dict(array=array,grid=grid,frequency=frequency)
        methods = [
            beamlib.beamforming.ConventionalBF(**beamforming_args),
            beamlib.beamforming.DAMAS(**beamforming_args),
            beamlib.beamforming.CLEAN_SC(**beamforming_args)
        ]
        
        database = np.load(
            f'{self.database_folder:}/' +
            self.database_format.format(case_name))
        data = database['data']
        CSMs = database['CSMs']

        if number_cases == 0: number_cases = data.shape[0]

        beamforming_maps = data[:number_cases,0,:,:]
        source_maps = data[:number_cases,1,:,:]

        # using the database tensors as is to eval the model only once
        nn_input = torch.tensor(
            beamforming_maps,dtype=torch.float32).unsqueeze(1)
        nn_maps = self.model.net(nn_input)
        nn_maps = nn_maps.detach().numpy()

        if number_cases < workers: workers = number_cases
        
        list_cases = deconvnetlib.tools.split_cases(
            number_cases,workers,cumulative=True)
        
        print('\nEvaluating in parallel...')
        print(f'number of cases: {number_cases:d}')
        print(f'number of workers: {workers:d}')
        print('cases distribution:')
        for index_worker, cases in enumerate(list_cases):
            print(f'\t{index_worker}: {cases}')
        print('\n')

        results_path = self.folders['results']
        figures_path = self.folders['figs']
        
        filename_format = \
            f'{figures_path:}/comparison_{case_name:}_f{frequency:06d}Hz_' + \
            r'{:06d}.png'

        with multiprocessing.managers.SharedMemoryManager() as smm:
            # sharing variables
            shared_source_maps = deconvnetlib.tools.SharedArr(smm, source_maps)
            shared_nn_maps = deconvnetlib.tools.SharedArr(smm, nn_maps)
            shared_CSMs = deconvnetlib.tools.SharedArr(smm, CSMs)

            fun = functools.partial(
                _compare_methods,
                methods=methods,
                grid=grid,
                shared_source_maps=shared_source_maps,
                shared_nn_maps=shared_nn_maps,
                shared_CSMs=shared_CSMs,
                filename_format=filename_format,
                threshold=level_threshold,
                loss_function=self.model.loss,
                **kwargs,
                )

            with multiprocessing.Pool(processes=workers,) as pool:
                results = pool.map(func=fun,iterable=list_cases,)
                    
        # merging and exporting the results to a csv file
        results = np.concatenate(results,axis=0)
        
        methods_names = [m.name for m in methods]
        methods_names.append('CNN')
        criterion_names = (
            'Loss','SourcesRatio','AvgDistance','AvgLevelDeviation')
        
        for index_criterion in range(results.shape[-1]):
            result_file = \
                f'{results_path:}/comparison_{case_name}_' + \
                f'{criterion_names[index_criterion]:}_f{frequency:06d}Hz.csv'
            deconvnetlib.tools.export_csv(
                result_file,
                names=methods_names,
                data=results[:,:,index_criterion].T,
            )
        
        print('\nDone\n')
        
        return results
        
        
def evaluate_model(model,data,filename_format,
                   name=None,plot=True,epsilon=0.0316):
    """ Evaluating the model for a given database
    
    Grid and array are not necessary since it only accounts for the
    disparity between the target and the estimated maps.
    
    ARGUMENTS
    ----------
    
        model: instance of the neural netowrk model, loaded and on eval mode
            
        data: a numpy array with a set of inputs and outputs, format , or 
            a path to numpy file (*.npz) with the data.
        
        filename_format: string with the path for saving the images. It must
            contain a field that will be replaced by a integer indicating the
            case index.
        
        name: string with the name to be used when saving the files. Optional,
            default is `None`, considers either the filename if `data` is
            a path and `test` otherwise.
        
        plot: boolean defining to plot the comparisons. Optional, default
            is `True`.
        
        epsilon: threshold for defining a point in a map as a source. Used to
            compare the number of expected and estimated sources. Optional,
            default is 0.0316 (corresponds to -15 dB).
    
    RETURNS
    ----------
    
        losses: tensor with the loss for each item of the batch.
        
        count_target: tensor with the number of sources in the target map.
        
        count_estimation: tensor with the number of sources in the
            deconvoluted map.
        
        ratios: tensor with ratio between the target and estimated number
            of sources.
        
    """

    # extracting the casename from the filename
    data_set = deconvnetlib.dataset.DeconvNetDataset(data)

    if name is None:
        if type(data) is str:
            name = data.split('/')[-1].replace('.npz','')
        else:
            name = 'test'
    print(f'\nEvaluating: {name:} ...\n')
    
    # considering big batch such as to get all cases
    data_loader = torch.utils.data.DataLoader(
        data_set,num_workers=4,batch_size=500)

    for batch in data_loader:
        x, y = batch
        y_hat = model.net(x)

        # calculating the loss for every individual case
        losses = torch.zeros(x.shape[0])
        count_target = losses.clone()
        count_estimation = losses.clone()

        for i, (target, estimation) in enumerate(zip(y,y_hat)):
            losses[i] = model.loss(
                estimation.unsqueeze(0), target.unsqueeze(0)
            )
            # ratio between the number of estimated sources and the number
            # of effective number of sources in the target map
            count_target[i] = int(torch.sum(target > epsilon))
            count_estimation[i] = int(torch.sum(estimation > epsilon))
            
        # generating a figure with the source map (target), beamforming
        # map (input) and deconvoluted map (output)
        if plot:
            fig, ax = deconvnetlib.visualize.plot_evaluation(
                x, y, y_hat, filename=filename_format)
    
    ratios = count_estimation/count_target
    
    print('\nDone\n')

    return losses, count_target, count_estimation, ratios


def _compare_methods(
    indexes,
    methods,loss_function,grid,
    shared_source_maps,shared_nn_maps,shared_CSMs,
    filename_format,
    threshold=-10,plot=True,footer='full'):
    """ Comparing the NN model to classical deconvolution methods
    
    It produces images with the classical beamforming map, the DAMAS and
    CLEAN-SC maps and the NN output map. To call it, SharedArr instances of 
    database and NN results are necessary
    
    This is a function prepared to be called on parallel using standard
    library `multiprocessing`
    
    TODO: 
    - make the criteria modulable, as inputs
    - put footer table in a function
    
    ARGUMENTS
    ----------
    
        indexes: list with the indexes of cases to be evaluated on the 
            function call.
            
        methods: list with the to be compared. only the solve method is called,
            using the default parameters
        
        loss_function: function used to calculate the loss
        
        grid: instance of the Grid, used to calculate the deconvolution with
            the canonical methods.
        
        shared_source_maps, shared_nn_maps, shared_CSMs:
            instances of ShareArr containing the source maps, neural network
            results and cross-spectral-matrix, respectively, as numpy arrays
            where the fist axis is the number of cases
        
        filename_format: string with the path for saving the images. It must
            contain a field that will be replaced by a integer indicating the
            case index.
        
        threshold: level in dB used relative to the most energetic point
            on map to distinguish a source. Optional, default is -10dB.

        plot: boolean defining to plot the comparisons. Optional, default
            is `True`.
        
        footer: string indicating the type of footer to be added at the 
            bottom of every map. Options (default is `full`):
               - 'empty': no footer
               - 'loss': only MSE loss is added
               - 'full': table with loss + 3 criteria
    
    RETURNS:
    ----------
    
        results: numpy array with the for each case 
            Format [num_cases,len(methods)+1,4], CNN is always last
    
    """
    
    colorbar_ticks = np.linspace(threshold,0,4)
    current_proc = multiprocessing.current_process()
    
    #beamforming_maps = shared_bf_maps.read() # recalculating the beamforming map
    source_maps = shared_source_maps.read()
    nn_maps = shared_nn_maps.read()
    CSMs = shared_CSMs.read()
    
    scan_grid_n = source_maps.shape[-1]
    grid_points = scan_grid_n*scan_grid_n
    
    # create a grid of indexes so the distance criteria is relative to the
    # number of cells
    vector_n = np.arange(scan_grid_n,dtype=float)
    grid_cols, grid_rows = np.meshgrid(vector_n,vector_n)
    grid_cols = grid_cols.reshape(grid_points)
    grid_rows = grid_rows.reshape(grid_points)
    
    calc_loss = lambda est, target: loss_function(
        torch.tensor(est).unsqueeze(0), torch.tensor(target).unsqueeze(0) )
    
    num_cases = len(indexes)
    num_methods = len(methods)+1 # add CNN
    
    if plot:
        ref_height = 4.5/2.54
        if footer == 'full': ref_height += 0.5/2.54
        fig, axs = plt.subplots(
            nrows=1,ncols=num_methods,
            figsize=((4*num_methods+1.5)/2.54,ref_height),
            sharex=True,sharey=True,
            constrained_layout=True
            )
    
        # 4 criteria (loss, cN, cD and cL)
        criteria_names = ('MSE','$c_N$','$c_D/\Delta x$','$c_L$, dB')
    
    results = np.zeros((num_cases,num_methods,4))
    
    for counter, case_ind in enumerate(indexes):
        
        first_plot = counter == 0
        
        source_map = source_maps[case_ind,:,:]
        source_map_list = source_map.reshape((grid_points,1))
        nn_map = nn_maps[case_ind,0,:,:]
        CSM = CSMs[case_ind]
        
        # selecting the sources location based on the map and the grid
        list_positions = np.arange(np.size(source_map_list))
        list_positions = np.expand_dims(list_positions,-1)
        
        sources_index = list_positions[source_map_list > 0]
        sources = np.array(
            [grid.coordinates[s,:] for s in sources_index])
        
        ###
        
        # normalizing the NN ap so the comparison is compatible to others
        nn_map_norm = nn_map/np.max(nn_map)
        # convert to dB
        nn_map_level = beamlib.beamforming.calculate_level(nn_map)
        
        nn_loss = calc_loss(nn_map_norm,source_map)
        sources_ratio, avg_distance, avg_level_deviation = \
            calculate_criteria(
                source_map,nn_map,
                grid_cols,grid_rows,
                threshold=threshold
                )
        results[counter,-1,:] = [
            nn_loss, sources_ratio, avg_distance, avg_level_deviation
            ]
        
        
        if first_plot and plot and footer == 'full':
            # due to a bug in automatic fontsize, creating all tables once
            # and with a reference text so it remains uniform for all maps
            footer_tables = []
            base_cells_text = [[r'$10 \times 10^{-10}$','000.00','00.00','00.00']]
            for ax in axs:
                footer_tables.append(
                    ax.table(
                        cellText=base_cells_text,
                        colWidths=[0.35,0.25,0.2,0.2],
                        rowLabels=[''],
                        colLabels=criteria_names,
                        loc='bottom',
                        cellLoc='center',edges='horizontal',
                        fontsize=9,
                        in_layout=False,
                        )
                    )
                footer_cells = footer_tables[-1].get_children()
                # forcing fontsize
                for cell in footer_cells:
                    cell.PAD = 0.01
                    cell.set_fontsize(9)
        
        # CNN model results is done first but plotted on last axis
        if plot:
            ax = axs[-1]
            loss_string = deconvnetlib.visualize.fancy_scientific(
                f'{nn_loss:.2e}')
            cells_text = [
                [loss_string,
                     *[f'{val:.2f}' for val in 
                      (sources_ratio, avg_distance, avg_level_deviation)
                      ]
                ]
            ]
        
            if first_plot:
                _, _, image_CNN = beamlib.visualize.surface.plot_grid(
                    grid.x,grid.y,nn_map_level,ax=ax)
                cbar = beamlib.visualize.surface.add_colorbar(
                    fig, image_CNN,
                    ax=ax, shrink=0.6, fraction=0.025, aspect=30)
                cbar.ax.yaxis.set_tick_params(pad=1)
                cbar.set_ticks(colorbar_ticks)
                cbar.ax.tick_params(length=2.0,width=0.5,labelsize=5)
                cbar.set_label(r'$\Delta$ source power, dB',size=6.5)
                
                # defining the colomap/colorbar limits
                image_CNN.set_clim([threshold,0])
                sources_CNN = ax.plot(
                    sources[:,0],sources[:,1],'xg',
                    linewidth=0.5,markersize=2.5)[0]
                ax.set_title('CNN', size=7.0, pad=3.0)
                ax.set_xticks([]), ax.set_yticks([])
    
                # list to store the classical deconvolution methods images
                # and sources plots
                image_convs, sources_convs = [], []
                
            else:
                image_CNN.set_data(beamlib.beamforming.calculate_level(nn_map))
                sources_CNN.set_data(sources[:,0],sources[:,1])
                
                
            # updating the footer
            if footer == 'loss':
                ax.set_xlabel(rf'MSE = {loss_string}',fontsize=8,)
            elif footer == 'full':
                footer_cells = footer_tables[-1].get_children()
                for cell, txt in zip(footer_cells[:4],cells_text[0]):
                    cell.get_text().set_text(txt)
        
        ###
        
        method_ind = 0
        for method in methods:
            
            # performing the beamforming calculation
            method.solve(CSM)
            
            # calculating the loss
            method_map = method.A/np.max(method.A)
            method_map = np.abs(method_map.reshape((scan_grid_n,scan_grid_n)))
            
            method_loss = calc_loss(method_map,source_map)
            
            sources_ratio, avg_distance, avg_level_deviation = \
                calculate_criteria(
                    source_map,method_map,
                    grid_cols,grid_rows,
                    threshold=threshold
                    )
            results[counter,method_ind,:] = [
                method_loss, sources_ratio, avg_distance, avg_level_deviation
            ]
            
            if plot:
                ax = axs[method_ind]
                
                loss_string = deconvnetlib.visualize.fancy_scientific(
                        f'{method_loss:.2e}')
                cells_text = [
                    [loss_string,
                         *[f'{val:.2f}' for val in 
                          (sources_ratio, avg_distance, avg_level_deviation)
                          ]
                    ]
                ]
                
                # plotting the result
                if first_plot:
                    _, _, mappable = beamlib.visualize.surface.plot_grid(
                        grid.x,grid.y,method.level,ax=ax)
                    image_convs.append(mappable)
                    # plotting the sources positions
                    sources_convs.append(
                        ax.plot(
                            sources[:,0],sources[:,1],'xg',
                            linewidth=0.5,markersize=2.5)[0]
                    )
                    # defining the colomap/colorbar limits
                    mappable.set_clim([threshold,0])
                    
                    ax.set_title(method.name, size=7.0, pad=3.0)
                    ax.set_xticks([]), ax.set_yticks([])
                else:
                    image_convs[method_ind].set_data(
                        method.level.reshape(scan_grid_n,scan_grid_n))
                    sources_convs[method_ind].set_data(
                        sources[:,0],sources[:,1])
                    
                # updating the footer
                if footer == 'loss':
                    ax.set_xlabel(rf'MSE = {loss_string}',fontsize=8,)
                elif footer == 'full':
                    footer_cells = footer_tables[method_ind].get_children()
                    for cell, txt in zip(footer_cells[:4],cells_text[0]):
                        cell.get_text().set_text(txt)
                                
            method_ind += 1
        
        if plot:
            figure_filename = filename_format.format(case_ind)
            print(f'{current_proc._identity[0]} - {figure_filename}',flush=True)
            fig.savefig(figure_filename, dpi=400, transparent=True)
        else:
            print(f'{current_proc._identity[0]} - {case_ind}',flush=True)
    
    return results


def calculate_criteria(ref_map,deconv_map,X,Y,threshold=-15):
    """ Evaluate criterion for quantifying the similarity of deconvoluted maps
    
    The criteria are the ratio of sources (estimated/actual), the average
    distance between the estimated and actual sources, and the average level
    deviation between the estimated and actual sources. For the later 2 
    criteria, algoritm finds the closer source. It can be that multiple
    deconvoluted sources are compared to the same reference source.
    
    ARGUMENTS
    ----------
    
        ref_map: sources map in Pa²
        
        deconv_map: deconvoluted map in Pa²
        
        X,Y: 1D arrays indicating the coordinates of the points. Used to
            calculate the distance criteria. Use indexes for having the result 
            in number of mesh points.
        
        threshold: level in dB used relative to the most energetic point
            on map to distinguish a source. Optinal, default is -15 dB
    
    RETURNS
    ---------
    
        sources_ratio: number of sources in the deconvoluted map divided by
            the number of sources in the reference map
            
        avg_distance: average distance between the sources in the deconvoluted
            map and the number of sources in the reference map
            
        avg_level_deviation: average level deviation between the sources in
            the deconvoluted map and the sources in the reference map
    
    """
    
    ref_sources_ind, ref_sources_level, ref_sources_mask = get_sources(
        beamlib.beamforming.calculate_level(ref_map),threshold)
    
    deconv_sources_ind, deconv_sources_level, deconv_sources_mask = get_sources(
        beamlib.beamforming.calculate_level(deconv_map),threshold)
    
    # criterion 1 - number of sources
    ref_n_sources = len(ref_sources_ind)
    deconv_n_sources = len(deconv_sources_ind)
    
    sources_ratio = deconv_n_sources/ref_n_sources
    # criterion 2 - position of sources
    source_mapping, distances = map_sources(
        ref_sources_ind,deconv_sources_ind,X,Y)
    
    avg_distance = np.sum(distances)/len(ref_sources_ind)
    
    # criterion 3 - sources amplitude
    # the difficulty is that one source can be split in two! in this case,
    # considering both of them!
    n_to_compare = np.min([len(n) for n in (ref_sources_ind,deconv_sources_ind)])
    ref_amplitudes = ref_sources_level[:n_to_compare]
    dev_amplitudes = deconv_sources_level[:n_to_compare]
    
    delta_amplitudes = ref_amplitudes - dev_amplitudes
    avg_level_deviation = np.sum(delta_amplitudes)/len(ref_sources_ind)
    
    return sources_ratio, avg_distance, avg_level_deviation


def get_sources(field,threshold):
    """ Return amplitude and coordinates of points over a threshold """
    mask = np.zeros(field.shape,dtype=bool)
    indexes = np.arange(0,field.size).reshape(field.shape)
    mask[ field >= threshold] = True
    sources_indexes = indexes[mask]
    sources_values = field[mask]
    # sorting by level (highest to lowest)
    ind_sort = np.flip(np.argsort(sources_values))
    
    return sources_indexes[ind_sort], sources_values[ind_sort], mask


def map_sources(ref_sources,deconv_sources,X,Y):
    """ Associating the deconvoluted sources with the simulated sources """
    n_ref, n_deconv = len(ref_sources), len(deconv_sources)
    
    # limiting to only the number of sources or of located sources
    # (the minimun value)
    n_to_compare = np.min((n_ref,n_deconv))
    
    distances_matrix = np.zeros((n_ref,n_to_compare))
    for ind_dev, sol_index in enumerate(deconv_sources[:n_to_compare]):
        for ind_ref, ref_index in enumerate(ref_sources):
            distances_matrix[ind_ref,ind_dev] = np.sqrt(
                    np.sum(
                        [(coords[sol_index] - coords[ref_index])**2
                            for coords in (X, Y)]
                        )
                )
    source_mapping = np.argmin(distances_matrix[:n_to_compare,:],axis=1)
    distances = np.min(distances_matrix,axis=1)
    
    return source_mapping, distances


