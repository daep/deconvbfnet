#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Functions for parallelizing the calculations

"""

import os
import multiprocessing
import multiprocessing.shared_memory

import numpy as np


class SharedArr():
    """ Class used for sharing arrays when using multiprocessing

    More details in `create_shared_array` and `read_shared_array` functions
    descriptions

    """

    def __init__(self,smm,arr):
        # creating shared array
        self.shm, self.shape_sl, self.dtype = create_shared_array(smm,arr)

    def read(self):
        arr = read_shared_array((self.shm, self.shape_sl, self.dtype))
        return arr


def create_shared_array(smm,arr):
    """ Create a shared array for a given SharedMemoryManager
    
    For creating the manager:
        
        '''
        with multiprocessing.managers.SharedMemoryManager() as smm:
            [code here]
        '''
    
    ARGUMENTS
    ----------
    
        smm: SharedMemoryManager instance where the shared memory blocks
            will be initiated
            
        arr: numpy array to be shared
    
    RETURNS
    ----------
    
        (arr_shm, shape_sl, arr.dtype): tuple with all the variables
            necessary to reach the shared array:
            - arr_shm: SharedMemory instance with the 1D data
            - shape_sl: ShareableList with the original shape of array
            - arr.dtype: type of the data, used for reading
    
    """
    shape = arr.shape
    if type(shape) is not tuple:
        shape = shape.tolist()
    
    shape_sl = smm.ShareableList(shape)
    
    arr_shm = smm.SharedMemory(size=arr.nbytes)
    buffered_arr = np.ndarray(
        arr.size, dtype=arr.dtype, buffer=arr_shm.buf)
    # copy the original data into shared memory
    buffered_arr[:] = arr[:].reshape(arr.size)
    
    return arr_shm, shape_sl, arr.dtype


def read_shared_array(adress):
    """ Recover a shared array for a given SharedMemory and ShareableList
    
    Must be called within the shared memory manager. Arguments are as
    returned by `create_shared_array`
    
    ARGUMENTS
    ----------
    
        adress: tuple containing the information for reafing the array:
            - arr_shm: SharedMemory instance with the 1D data
            - shape_sl: ShareableList with the original shape of array
            - dtype: type of the data
        
    RETURNS
    ----------
    
        arr: numpy array
    
    """
    arr_shm, shape_sl, dtype = adress
    shape = list(shape_sl)
    arr = np.ndarray((np.prod(shape)), dtype=dtype, buffer=arr_shm.buf)
    arr = arr.reshape(shape)
    
    return arr


def split_cases(number_cases,number_workers,cumulative=False):
    """ Split number of cases equally among the workers

    ARGUMENTS
    ---------

        number_cases: integer with the number of cases
        number_workers: integer with the number of workers to split the cases
        cumulative: boolean, if True the returned list indicates a
            cumulative list. Optional, default is False

    RETURNS
    ----------

        cases_list: list with the number of cases per worker or with lists of
            indexes per worker (if `cumulative` is True)

    """ 
    cases_per_worker = number_cases // number_workers
    cases_list = [cases_per_worker for i in range(number_workers)]
    index_to_add = 0

    while sum(cases_list) < number_cases:
        if index_to_add > len(cases_list) - 1:
            index_to_add = 0
        else:
            index_to_add += 1
        cases_list[index_to_add] += 1

    if cumulative:
        cases_indexes = [np.arange(cases_list[0])]
        for n in cases_list[1:]:
            cases_indexes.append(
                cases_indexes[-1][-1] + np.arange(1,n+1,dtype=int)
                )
        cases_list = cases_indexes

    return cases_list


def export_csv(filename,names,data):
    """ Export 2D array or list of arrays to a CSV file
    
    First row is the header containing the name of the columns. First
    column is the row number.
    
    ARGUMENTS
    ----------
    
        filename: path of the folder to be generated
        
        names: column names
        
        data: 2D array of list of arrays or lists with the data to be saved.
            If different datatpyes, adapt the column width and the format
    
    RETURNS
    ----------
    
        ##none##
        
    """
    
    def types_dict(d):
        # select type of data
        try: # for torch only
            if d.is_floating_point():  size, fmt = 15, '15.6e'
            else: size, fmt = 8, '8d'
        except(AttributeError): # if not torch
            if isinstance(d,(float,np.float_)): size, fmt = 15, '15.6e'
            elif isinstance(d,(int,np.int_)): size, fmt = 8, '8d'
            else: raise ValueError(f'Data format {type(d)} unknown')
        return size, fmt
    
    formats = [types_dict(d[0]) for d in data]
       
    header_format = ['{:>' + str(fmt[0])  + '},' for fmt in formats]
    header_format = '{:>8},'+''.join(header_format)[:-1]
    
    data_format = ['{:' + fmt[1]  + '},' for fmt in formats]
    data_format = '{:8d},' + ''.join(data_format)[:-1]
    
    with open(filename,'w') as f:
        f.write(header_format.format('#',*names) + '\n')
        for ind, data_row in enumerate( zip(*data) ):
            f.write(data_format.format(ind,*data_row) + '\n')