#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Convolutional neural network to be used to deconvolute
beamforming maps.

Neural network architecture and callbacks

"""

import torch
from torch import nn
import torch.nn.functional as F
import pytorch_lightning as pl

import deconvnetlib.visualize
import deconvnetlib.modules


class DeconvNet(nn.Module):
    """ Network architecture

    Modulable, base on arguments given at start. 
    Consider `n_blocks` as the number of convolutional blocks

    ARGUMENTS
    ----------

        channels: list with `n_blocks - 1` items indicating the number of
            output channels for each convolution block. Ouput of the last
            block is not given since it must always be one.

        selected_paddings: list with `n_blocks - 1` items indicating the
            padding of each convolution block. Options are:

                0: replication padding
                1: reflection padding
                2: zero padding
                3: custom Tukey padding
                
            Optional, default is all 0 (replication padding). There is
            no padding for the final block.

        activated: list with `n_blocks` items indicating if the activation
            function (ReLU) is added after that given block of channels.

    """
    
    def __init__(self,
        shape,
        channels=[16,32,64,32,16,8],
        selected_paddings=[0,0,0,0,0,0],
        activated=[True,True,True,True,True,True,False]
    ):
        super().__init__()
        
        n_blocks = len(channels) + 1
        kernel_size = 3
        pad_size = kernel_size // 2
        # accounting for the first and last channels (always one)
        channels = [1,*channels,1]

        list_paddings = [
            nn.ReplicationPad2d(pad_size),
            nn.ReflectionPad2d(pad_size),
            nn.ZeroPad2d(pad_size),
            deconvnetlib.modules.TukeyPad2d(
                kernel_size=[shape[0]+2*pad_size]*2), 
        ]

        layers = []

        # add normalization at start
        layers.append(
            nn.InstanceNorm2d(
                num_features=1, eps=1e-10, affine=False)
        )

        for i in range(n_blocks-1):
            block = [
                list_paddings[selected_paddings[i]],
                nn.Conv2d(
                    channels[i], channels[i+1],
                    kernel_size=kernel_size, padding=0,
                )
            ]
            if activated[i]:
                block.append(nn.ReLU(inplace=True))
            
            [layers.append(operation) for operation in block]

        # adding final block
        layers.append(
            nn.Conv2d(
                channels[-2], channels[-1],
                kernel_size=1, padding=0,
            )
        )
        if activated[-1]:
            layers.append(nn.ReLU(inplace=True))

        self.encode = nn.Sequential(*layers)

    def forward(self, x):
        return self.encode(x)

    def summary(self):
        info = str(self)
        print(f'\nNeural network architecture:\n\n{info}\n\n')


class DeconvNetModule(pl.LightningModule):
    """ Lightning module with the neural network

    ARGUMENTS
    ----------

        net: neural network (torch's nn.Module class instance)

        hparams: dict with hyperparamereters and training properties that will
            be available during all steps. Keys:
            - `experiment_folder`: folder where to store the experiment
            - `results_folder`: folder where to store the results
            - `learning_rate`: training learning rate

    """

    def __init__(self,net,hparams):
        super().__init__()
        self.hparams = hparams
        self.net = net
        self.loss = torch.nn.MSELoss(reduction='mean')
        self.learning_rate = hparams['learning_rate']

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(
            self.parameters(),
            lr=self.learning_rate,
            weight_decay=0)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer, mode='min',
            factor=0.1, patience=10,
            threshold=0.0001, threshold_mode='rel',
            cooldown=0, min_lr=0, eps=1e-08,
            verbose=True)
        return dict(
            optimizer=optimizer,scheduler=scheduler,monitor='train_loss')
    
    def _eval(self, batch, batch_idx):
        x, y = batch
        y_hat = self.net(x)
        loss = self.loss(y_hat, y)
        return loss
    
    def training_step(self, batch, batch_idx):
        loss = self._eval(batch, batch_idx)
        self.log('train_loss', loss)
        return loss
    
    def validation_step(self, batch, batch_idx):
        loss = self._eval(batch, batch_idx)
        self.log('val_loss', loss)
        return loss

    def test_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.net(x)
        loss = self.loss(y_hat, y)

        # calculating the loss for every individual case
        losses = torch.zeros(x.shape[0])
        ratios = losses.clone()
        epsilon = 1e-2 # criteria to define a source

        for i, (target, estimation) in enumerate(zip(y,y_hat)):
            
            losses[i] = self.loss(
                estimation.unsqueeze(0), target.unsqueeze(0)
            )
            # ratio between the number of estimated sources and the number
            # of effective number of sources in the target map
            n_sources_target = torch.sum(target > epsilon)
            n_sources_estimation = torch.sum(estimation > epsilon)
            ratios[i] = n_sources_estimation/n_sources_target
        
        # generating a figure with the source map (target), beamforming map
        # (input) and deconvoluted map (output)
        fig, ax = deconvnetlib.visualize.plot_evaluation(
            x, y, y_hat,
            filename=self.hparams['results_folder']+'/test_result_{:06d}')
        
        self.log('test_loss', [losses])
        self.log('test_count', [ratios])

        return loss