#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Dataset definition

"""

import numpy as np

import torch
from torch.utils.data import Dataset

class DeconvNetDataset(Dataset):
    """ Dataset class for the beamforming cases.

    At instancing, data is loaded, converted to torch tensors and
    normalized.

    ARGUMENTS
    ----------

        data: numpy array in format [N,2,n,n], where `N` the is
            number of cases (pairs) and `n` is the number of points for the 
            scan discretization. First dimension of second axis is for the
            input (beamforming map) and second for the target (source 
            amplitudes map)
            
            OR
            
            a path for a *.npz file containing the data
        
        normalize: boolean indicating if input data (beamforming maps)
            should be normalized. Optional, default is `False`

    """

    def __init__(self,data,normalize=False):

        if type(data) is str:
            data_np = np.load(data)['data']
        else:
            data_np = data

        n_cases, _, grid_nx, grid_y = data_np.shape
        
        # splitting the different cases
        # batch, (in, output), channel, nx, ny
        data_pt = torch.tensor(data_np, dtype=torch.float32)
        # adding extra dimension (single channel)
        data_pt = data_pt.unsqueeze(2)
        
        # normalizing the beamforming map
        if normalize:
            norm = lambda X: (X - torch.mean(X))/torch.max(torch.abs(X))
            # doing in a loop so I'm sure it works, later do it smarter
            for n in range(n_cases):
                data_pt[n,0,0,:,:] = norm(data_pt[n,0,0,:,:])
            
        self.data = data_pt
    
    def __len__(self):
        return self.data.shape[0]
    
    def __getitem__(self, idx):
        return self.data[idx,0], self.data[idx,1]

