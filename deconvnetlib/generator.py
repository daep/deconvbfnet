#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Classes and functions for creating a database of source and beamforming maps.

It uses Beamlib's implementations of the beamforming algorithms to calculate
the conventional beamforming map (input of the neural network) and the
cross-spectral matrix (CSM), later used to compare the neural network
results with classical deconvolution methods.

Beamlib is available at:
    https://gitlab.isae-supaero.fr/acoustic-beamforming/beamlib

"""

import functools
import multiprocessing

import numpy as np

import deconvnetlib
import beamlib


class Generator():
    
    def __init__(self,database_folder=None,typ='incoherent',
                 grid=None,array=None,size=21,
                 c0=343):
        """ Class used to generate a database
        
        Default parameters will produce a database corresponding to 
        incoherent sources placed on a 21 x 21 scanning grid. Signals will be
        generated for an Underbrink array of 65 microphones with diameter
        of 1.0m, placed 1.2 meters away from the focal plan.
        
        ----------
        ARGUMENTS
        
            database_folder: string with the path where to save the database.
                Optional, default (None) will consider subfolder `database`
                of current working folder.
            
            type: type of sources. Only 'tonal' and 'incoherence' monopoles
                are available
            
            grid: scanning grid. Either an instance of a Beamlib's grid object
                or a path to a `grid.xml` file. Creates an uniform grid by
                default (None).
            
            array: microphone arra. Either an instance of a Beamlib's array
                object or a path to an `array.xml` file. Creates an
                Underbrink array by default (None).
            
            size: integer with dicretization of the scanning grid.
                Default is 21 by 21 (uniform, square grid).
            
            c0: sound speed in m/s. Optional, default is 343 m/s.
        
        """
        
        self.database_folder = database_folder
        
        if grid is None:
            # uniform square mesh, from -0.5 to 0.5 with `size` points on
            # each side, placed at 1.2 meters from the focal antenna
            grid = beamlib.grid.Uniform(
                n=size,x=[-0.5,0.5],y=[-0.5,0.5],z=1.2)
            # export scanning grid in xml format`
            grid.export(f'{self.database_folder}/grid.xml')
        self.grid = grid
    
        if array is None:
            # Underbrink with 65 microphones
            array = beamlib.arrays.Underbrink(
                Na=8, Nm=8, v=5*np.pi/16, r0=0.1, rmax=0.5,
                array_center=[0,0], z=0.0, center_mic=True,
                )
            # export array in xml format
            array.export(f'{self.database_folder}/array.xml')
        self.array = array
        
        if typ == 'incoherent':
            self.signal_generator = beamlib.signal.IncoherentMonopole(
                array=array,sources=[grid.coordinates[0,:]],
                c0=c0,amplitudes=1.0,
                )
        elif typ == 'tonal':
            self.signal_generator = beamlib.signal.TonalMonopole(
                array=array,
                sources=[grid.coordinates[0,:]],
                frequencies=2000,
                )
        else:
            raise ValueError(f'Type of source {typ} undefined')
        
        
    def random(self,filename,number_cases,range_number,range_level,
               frequency=2000,workers=5):
        """ Generate a database with randomly placed sources """
        
        frequency = int(frequency)

        beamformer = beamlib.beamforming.ConventionalBF(
            array=self.array,grid=self.grid,frequency=frequency)
        
        # calculating the database in parallel
        calc_beam = functools.partial(
            _calc_random,
            range_number=range_number,
            range_level=range_level,
            frequency=frequency,
            beamformer=beamformer,
            signal_generator=self.signal_generator,
            grid=self.grid,
            )
        # split the cases equally among the workers
        cases_list = deconvnetlib.tools.split_cases(
            number_cases,workers,cumulative=False)
        
        with multiprocessing.Pool(processes=workers) as pool:
            results = pool.map(func=calc_beam, iterable=cases_list,)
        
        # concatenating input (beamformed map) and output (source map)
        index_case = 0
        data = np.zeros(
            (number_cases,2,self.grid.x.size,self.grid.y.size),
            dtype=np.float64,
            )
        CSMs = np.zeros(
            (number_cases,self.array.number_microphones,self.array.number_microphones),
            dtype=np.complex128,
            )
        for result in results:
            for beam_map, source_map, CSM in result:
                data[index_case,0,:,:] = beam_map
                data[index_case,1,:,:] = source_map
                CSMs[index_case,:,:] = CSM
    
                index_case += 1
        
        path = f'{self.database_folder:}/{filename}_f{frequency:05d}Hz.npz'
        print(f'\nSaving file\n {path}')
        np.savez(path,data=data,CSMs=CSMs,
                 grid=self.grid,array=self.array,
                 frequency=frequency)
        
        print('\ndone\n')
    
    
    def fixed(self,filename,coordinates,amplitudes,frequency=2000,workers=5,):
        """ Generate a database with arbitrarly placed sources """
        
        frequency = int(frequency)

        beamformer = beamlib.beamforming.ConventionalBF(
            array=self.array,grid=self.grid,frequency=frequency)
        
        # split the cases equally among the workers
        number_cases = len(coordinates)
        if workers > number_cases: workers = number_cases
        
        cases_list = deconvnetlib.tools.split_cases(
            number_cases,workers,cumulative=True)
        
        # selecting the corresponding list of sources and amplitudes
        # to be given to each worker
        sources_list = []
        for cases_per_worker in cases_list:
            sources_list.append(
                [[coordinates[index_case] for index_case in cases_per_worker],
                 [amplitudes[index_case] for index_case in cases_per_worker]],
                )
        
        calc_beam = functools.partial(
            _calc_list,
            frequency=frequency,
            beamformer=beamformer,
            grid=self.grid,
            signal_generator=self.signal_generator,
            )
        
        with multiprocessing.Pool(processes=workers) as pool:
            results = pool.map(func=calc_beam, iterable=sources_list,)
        
        # concatenating input (beamformed map) and output (source map)
        index_case = 0
        data = np.zeros(
            (number_cases,2,self.grid.x.size,self.grid.y.size),dtype=np.float64)
        CSMs = np.zeros(
            (number_cases,self.array.number_microphones,self.array.number_microphones),
            dtype=np.complex128,
            )
        for result in results:
            for beam_map, source_map, CSM in result:
                data[index_case,0,:,:] = beam_map
                data[index_case,1,:,:] = source_map
                CSMs[index_case,:,:] = CSM
                
                index_case += 1
            
        path = f'{self.database_folder:}/{filename}_f{frequency:05d}Hz.npz'
        print(f'\nSaving file\n {path}')
        np.savez(path,data=data,CSMs=CSMs,
                 grid=self.grid,array=self.array,
                 frequency=frequency)
            
        print('\ndone\n')
        
        


def _calc_random(n_cases,range_number,range_level,
                 frequency,beamformer,grid,signal_generator,):
    """ Calculate input/output maps randomly
    
    TODO: Add a mean to make determinisitic runs so one can recreate the
        same random database
    
    ARGUMENTS
    ----------
    
        n_cases: number of input-output fields to be generated
        
        range_number: tuple with min-max number of sources
        
        range_level: possible variation of amplitudes in dB. If 0, all sources
            will have the same amplitude
        
        frequency: beamforming frequency in Hz
        
        beamformer: beamlib's beamformer instance that will be used to 
            calculate the beamforming maps
        
        grid: grid class instance
        
        signal_generator: signal class with used to generate the signals
    
    RETURNS
    ----------
    
        cases: list containing the input (beamforming) and output (sources)
            maps and the cross-spectral matrix (CSM)
    
    """
    
    # Since it is callled using multiprocessing, the initial state is the same
    # for all subprocesses.In order to generate different cases on each one of
    # them, the random number seed must be changed. Using the process PID as
    # seed for the random number generator
    process = multiprocessing.current_process()
    np.random.seed(process.pid)
    
    number_scan_points = grid.N
    scan_grid_shape = [int(np.sqrt(number_scan_points))]*2
    
    worker_number = int(multiprocessing.current_process().name.split('-')[-1])
    
    cases = []
    for index_case in range(n_cases):
        # number of sources following an uniform distribution
        n_sources = np.random.randint(
            low=range_number[0],high=range_number[1]+1)
        # selecting points using uniform distribution, no replacement
        index_sources = np.random.choice(
            number_scan_points, size=n_sources, replace=False)
        
        sources_pos = grid.coordinates[index_sources,:]
        
        source_map = np.zeros((number_scan_points))
        for counter, ind in enumerate(index_sources):
            
            # since the levels are relative, the first source level is
            # always fixed as 1 and the remaining ones are proportional to it
            if counter == 0:
                amp = 1.0
            else:
                amp_level = np.random.uniform(low=-np.abs(range_level),high=0)
                amp = 10**(amp_level/10)
            
            source_map[ind] = amp
            
        source_map = source_map.reshape(scan_grid_shape)

        # coherent sources
        # signal_generator.update(sources_pos,frequencies=2000)
        signal_generator.update(sources_pos)
        # calculate cross-spectral matrix (CSM)
        signal_generator.calc_CSM(frequency=frequency,nfft=1024)
        
        # performing the beamforming calculation
        beam_map = beamformer.solve(signal_generator.CSM)
        beam_map = np.abs(beam_map).reshape(scan_grid_shape)
    
        if index_case % 10 == 0:
            print(f'{worker_number:d} - {index_case:05d}/{n_cases:05d}')
        
        cases.append((beam_map, source_map, signal_generator.CSM))
    
    return cases


def _calc_list(sources,frequency,beamformer,grid,signal_generator):
    """ Calculate input/output maps for a predefined list of sources
    
    ARGUMENTS
    ----------
    
        sources: list with the sources coordinates and amplitudes
            for each case
        
        frequency: frequency of the beamforming, Hz
        
        beamformer: beamlib's beamformer instance that will be used to 
            calculate the beamforming maps
        
        grid: grid class instance
        
        signal_generator: signal class with used to generate the signals
    
    RETURNS
    ----------
    
        cases: list containing the input (beamforming) and output (sources)
            maps and the cross-spectral matrix (CSM)
    
    """
    
    worker_number = int(multiprocessing.current_process().name.split('-')[-1])
    
    number_scan_points = grid.N
    scan_grid_shape = [grid.x.size,grid.y.size]
    
    coordinates, amplitudes = sources
    n_cases = len(coordinates)
    
    cases = []
    for index_case, (case_sources, case_amplitudes) in enumerate(
            zip(coordinates,amplitudes)):
        
        source_map = np.zeros((number_scan_points))
        
        for pos, amp in zip(case_sources,case_amplitudes):
            
            index_source = np.argwhere((grid.coordinates == pos).all(axis=1))
            source_map[index_source] = amp
        
        source_map = source_map.reshape(scan_grid_shape)
        
        # coherent sources
        signal_generator.update(case_sources,amplitudes=case_amplitudes)
        # calculate cross-spectral matrix (CSM)
        signal_generator.calc_CSM(frequency=frequency,nfft=1024)
        
        # performing the beamforming calculation
        beam_map = beamformer.solve(signal_generator.CSM)
        beam_map = np.abs(beam_map).reshape(scan_grid_shape)
    
        if index_case % 10 == 0:
            print(f'{worker_number:d} - {index_case:05d}/{n_cases:05d}')
        
        cases.append((beam_map, source_map, signal_generator.CSM))
    
    return cases
