#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Module with functions used to load a model

"""

import os
import yaml

import torch

import deconvnetlib.network


def load_model(experiment_folder=None,parameters_file=None,
    checkpoint_file=None,mkdirs=True,verbose=False,device='cpu'):
    """ Load a saved checkpoint and returns the model
    
    ARGUMENTS
    ----------
    
        experiment_folder: path to the experiment. By default (None), considers
            the experiment folder as the directory where the script was called.

        parameters_file: path to the file containing the model paramerters.
            By default (None), considers the `parameters.yaml` available in
            the experiments folder.

        checkpoint_file: path to the file containing the checkpoint (model's
            state). By default (None), considers the `best.ckpt` available in
            the `<experiment_folder>/ckpt` subfolder.

        mkdirs: create subfolders for the results if not already there.
            Optional, default is True.

        verbose: boolean indicating if the neural nework architecture
            should be displyed after loading. Optional, default is False.
            
        device: string with the device where to load the model. It can be
            either 'cpu' or 'cuda'. Optional, default is 'cpu'

    RETURNS
    ----------

        model: instance of the model (LightningModule children) at the state
            stored in checkpoint.

        parameters: dictionary with the neural network parameters

        folders: dictionary with the default paths to the figures, results and      
            checkpoints subfolders.

    """

    if experiment_folder is None:
        experiment_folder = os.getcwd()
    
    if parameters_file is None:
        parameters_file = experiment_folder + '/parameters.yaml'
    with open(parameters_file) as file:
        parameters = yaml.load(file, Loader=yaml.FullLoader)

    folders = dict(
        exp=f'{experiment_folder}',
        figs=f'{experiment_folder}/figs/',
        results=f'{experiment_folder}/results/',
        ckpts=f'{experiment_folder}/ckpts/',
    )
    if mkdirs:
        for _, folder in folders.items():
            os.makedirs(folder, exist_ok=True)

    net = deconvnetlib.network.DeconvNet(**parameters)

    # displaying architecture on terminal
    if verbose: net.summary()

    if checkpoint_file is None:
        checkpoint_file = folders['ckpts'] + 'best.ckpt'

    model = deconvnetlib.network.DeconvNetModule.load_from_checkpoint(
        checkpoint_file,net=net,map_location=torch.device(device))
    model.eval()

    return model, parameters, folders