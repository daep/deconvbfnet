#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Callbacks used to follow the training convergence

"""

import numpy as np

import torch
from torch import nn
import torch.nn.functional as F

import pytorch_lightning as pl
from pytorch_lightning.callbacks import Callback

import deconvnetlib.visualize


class PrintCallback(Callback):
    
    def __init__(self, folder):
        self.destination_folder = folder
    
    def on_train_start(self, trainer, pl_module):
        print('\nstart\n')
        print('{:^8} {:^16} {:^16} {:^16}'.format(
            'epoch', 'train_loss', 'val_loss', 'val/train'))
        self.convergence = []
    
    def on_train_epoch_end(self, trainer, pl_module, outputs):
        train_loss = trainer.logged_metrics['train_loss'].cpu() # seding to cpu!
        val_loss = trainer.logged_metrics['val_loss'].cpu()
        values = [
            pl_module.current_epoch, 
            train_loss,
            val_loss,
            val_loss/train_loss,
            ]
        print('  {:06d} {:16.8e} {:16.8e} {:16.4f}'.format(*values))
        self.convergence.append(values)
    
    def on_train_end(self, trainer, pl_module):
        values = np.array(self.convergence)
        
        # exporint a csv file with the evolution of the losses
        deconvnetlib.tools.export_csv(
            filename=f'{self.destination_folder:}/data_convergence.csv',
            names=['epoch','train_loss','valid_loss','ratio'],
            data=values.T)

        # ploting and saving a graph with the loss convergence
        fig, ax = deconvnetlib.visualize.plot_convergence(
            values[:,0],values[:,1],values[:,2])    
        fig.savefig(
            f'{self.destination_folder:}/graph_convergence.png',dpi=300)

        print('\nend\n')
