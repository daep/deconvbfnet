#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# neural network base functions and classes
import deconvnetlib.callbacks
import deconvnetlib.dataset
import deconvnetlib.modules
import deconvnetlib.network

# acessory functions for maps and checkpoints
import deconvnetlib.loader
import deconvnetlib.tester
import deconvnetlib.tools
import deconvnetlib.visualize

# for database generation
import deconvnetlib.generator
import deconvnetlib.benchmarks


import os
from multiprocessing import current_process

def check_version():
    """ Displays deconvnetlib's version on the terminal """
    is_main = current_process().name == 'MainProcess' 
    if is_main:
        VERSION = os.popen(
            'git --git-dir={:}/../.git describe --tags'.format(
                os.path.dirname(__file__))
                ).read()
        print(f'Deconvnetlib version: {VERSION}')