#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Classes for defining the source maps for benchmark cases (source 
moving from center to border, two sources, etc...) for a given grid.

Each one of them must contain the method `generate`, where the sources
coordinates and amplitudes are defined for a given `grid`.

"""

import numpy as np


class BenchmarksBase():
    """ Base class """
    def __len__(self):
        return len(self.coordinates)
        
    def plot(self,grid):
        import matplotlib.pyplot as plt
        fig, axs = plt.subplots(1,len(self),sharex=True,sharey=True)
        if len(self) == 1: axs = [axs]
        for i in range(len(self)):
            axs[i].plot(grid.coordinates[:,0],grid.coordinates[:,1],'+')
            for coords in self.coordinates[i]:
                axs[i].plot(coords[0],coords[1],'ok')
            axs[i].set_aspect(1.0)


class SingleSource(BenchmarksBase):
    """ Single source moving from center to edge """
    
    def __init__(self,):
        self.name = 'SingleSource'
    
    def generate(self,grid,z=1.2):
        center_index = [grid.x.size // 2, grid.y.size // 2]
        self.coordinates, self.amplitudes = [], []
        for delta_x in range(grid.x.size - center_index[0]):
            self.coordinates.append(
                np.array([
                    [grid.x[center_index[0] + delta_x],
                     grid.y[center_index[1]],
                     z]
                ])
            )
            self.amplitudes.append([1.0])
            
        return [self.coordinates,self.amplitudes]
    
    
class TwoSources(BenchmarksBase):
    """ Single source at the center with another source moving to border """
    
    def __init__(self,):
        self.name = 'TwoSources'
    
    def generate(self,grid,z=1.2):
        center_index = [grid.x.size // 2, grid.y.size // 2]
        self.coordinates, self.amplitudes = [], []
        for delta_x in range(1,grid.x.size - center_index[0]):
            self.coordinates.append(
                np.array([
                    [grid.x[center_index[0]],
                     grid.y[center_index[1]],
                     z],
                    [grid.x[center_index[0] + delta_x],
                     grid.y[center_index[1]],
                     z]
                ])
            )
            self.amplitudes.append([1.0,1.0])
            
        return [self.coordinates,self.amplitudes]
    
    
class TwinSources(BenchmarksBase):
    """ Two sources moving to border """
    
    def __init__(self,):
        self.name = 'TwinSources'
    
    def generate(self,grid,z=1.2,distance=2):
        center_index = [grid.x.size // 2, grid.y.size // 2]
        self.coordinates, self.amplitudes = [], []
        for delta_x in range(1,grid.x.size - center_index[0] - distance):
            self.coordinates.append(
                np.array([
                    [grid.x[center_index[0] + delta_x],
                     grid.y[center_index[1]],
                     z],
                    [grid.x[center_index[0] + delta_x + distance],
                     grid.y[center_index[1]],
                     z]
                ])
            )
            self.amplitudes.append([1.0,1.0])
            
        return [self.coordinates,self.amplitudes]
    
    
class Circular(BenchmarksBase):
    """ Circular sources of increasing radius """
    
    def __init__(self,):
        self.name = 'Circular'
    
    def generate(self,grid,z=1.2):
        center_index = [grid.x.size // 2, grid.y.size // 2]
        max_radius = min(center_index)
        
        # array with distance to center
        X, Y = np.meshgrid(grid.x,grid.y)
        arr_distance = np.sqrt(X**2 + Y**2)
        dx = grid.x[1] - grid.x[0]
        arr_distance /= dx
        
        arr_distance = arr_distance.reshape(grid.N)
        X, Y = X.reshape(grid.N), Y.reshape(grid.N)
        
        indexes = np.arange(grid.N)
        
        self.coordinates, self.amplitudes = [], []
        for radius in range(max_radius+1):
            sources_mask = np.abs(arr_distance - radius) <= 0.5
            sources_index = indexes[sources_mask]
            n_sources = len(sources_index)
            
            self.coordinates.append(
                np.array(
                   (X[sources_index],Y[sources_index],[z]*n_sources),
                ).T
            )
            self.amplitudes.append([1.0]*n_sources)
            
        return [self.coordinates,self.amplitudes]
    
    
class Line(BenchmarksBase):
    """ Aligned sources for increasing length """
    
    def __init__(self,):
        self.name = 'Line'

    def generate(self,grid,z=1.2):
        center_index = [grid.x.size // 2, grid.y.size // 2]
        self.coordinates, self.amplitudes = [], []
        for delta_x in range(grid.x.size - center_index[0]):
            n_sources = 1 + delta_x*2
            self.coordinates.append(
                np.array([
                    grid.x[center_index[0]-delta_x:center_index[0]+delta_x+1],
                    [grid.y[center_index[1]]]*n_sources,
                    [z]*n_sources
                ]).T
            )
            self.amplitudes.append([1.0]*n_sources)
        return [self.coordinates,self.amplitudes]
    
    
class RotatingLine(BenchmarksBase):
    """ Linear distribution for complete grid, rotating """
    
    def __init__(self,):
        self.name = 'RotatingLine'

    def generate(self,grid,n=10,z=1.2):
        
        center_index = [grid.x.size // 2, grid.y.size // 2]
        dx = grid.x[1] - grid.x[0]
        
        indexes = np.arange(grid.N)
        
        # array with distance to center
        X, Y = np.meshgrid(grid.x,grid.y)
        X_line, Y_line = X.reshape(grid.N), Y.reshape(grid.N)
            
        # coefficients of line equation
        a, c = 1, 0
        
        self.coordinates, self.amplitudes = [], []
        angles = np.linspace(0,np.pi,n+1)[:-1] # avoid repeating first
        for angle in angles:
            # b coefficients for the giving angle
            if angle == 0:
                b = 1.0/np.tan(np.pi)
            else:
                b = -1.0/np.tan(angle)
                
            ab_square = np.sqrt(a**2 +b**2)
            
            delta_X = (X - grid.x[center_index[0]])
            delta_Y = (Y - grid.y[center_index[1]])
            
            # distance from points (x, y) for line a*x + b*y + c = 0
            arr_distance = np.abs(a*delta_X + b*delta_Y + c)/ab_square
            arr_distance = arr_distance.reshape(grid.N) / dx
            
            sources_mask = np.abs(arr_distance) <= 0.5
            sources_index = indexes[sources_mask]
            n_sources = len(sources_index)
            
            self.coordinates.append(
                np.array(
                   (X_line[sources_index],Y_line[sources_index],[z]*n_sources),
                ).T
            )
            self.amplitudes.append([1.0]*n_sources)
            
        return [self.coordinates,self.amplitudes]
    
    
class ISAE(BenchmarksBase):
    """ Letter ISAE, in reference to ISAE-SUPAERO """
    
    def __init__(self,):
        self.name = 'ISAE'

    def generate(self,grid,z=1.2):
        
        # letter height, width and distance in number of grid points
        h, w, d =  7, 3, 1
        half_h, half_w, half_d = h//2, w//2, d//2
        
        center_index = [grid.x.size // 2, grid.y.size // 2]
        grid_mask = np.zeros((grid.x.size,grid.x.size),dtype=bool)
        
        # letter center
        lc = np.array(
            [center_index[0] - ((half_d + w) + (d + half_w) + 1),center_index[1]]
            )
        
        # I
        grid_mask[lc[0],lc[1]-half_h:lc[1]+half_h+1] = True
        grid_mask[lc[0]-half_w:lc[0]+half_w+1,lc[1]+half_h] = True
        grid_mask[lc[0]-half_w:lc[0]+half_w+1,lc[1]-half_h] = True
        
        # S
        lc[0] += w + d
        grid_mask[lc[0]-half_w:lc[0]+half_w+1,lc[1]] = True
        grid_mask[lc[0]-half_w:lc[0]+half_w+1,lc[1]+half_h] = True
        grid_mask[lc[0]-half_w:lc[0]+half_w+1,lc[1]-half_h] = True
        grid_mask[lc[0]-half_w,lc[1]:lc[1]+half_h] = True
        grid_mask[lc[0]+half_w,lc[1]-half_h:lc[1]] = True
        
        # A
        lc[0] += w + d
        grid_mask[lc[0]-half_w:lc[0]+half_w+1,lc[1]] = True
        grid_mask[lc[0]-half_w:lc[0]+half_w+1,lc[1]+half_h] = True
        grid_mask[lc[0]-half_w,lc[1]-half_h:lc[1]+half_h] = True
        grid_mask[lc[0]+half_w,lc[1]-half_h:lc[1]+half_h] = True
        
        # E
        lc[0] += w + d
        grid_mask[lc[0]-half_w:lc[0]+half_w+1,lc[1]+half_h] = True
        grid_mask[lc[0]-half_w:lc[0]+half_w+1,lc[1]] = True
        grid_mask[lc[0]-half_w:lc[0]+half_w+1,lc[1]-half_h] = True
        grid_mask[lc[0]-half_w,lc[1]-half_h:lc[1]+half_h] = True
        
        indexes = np.arange(grid.N)
        grid_mask = grid_mask.T.reshape(grid.N)
        
        self.coordinates = grid.coordinates[indexes[grid_mask],:-1]
        n_sources = self.coordinates.shape[0]
        zs = z*np.ones((n_sources,1))
        self.coordinates = np.hstack((self.coordinates,zs))
                
        # putting it in a list to make it compatible to others
        self.coordinates = [self.coordinates.tolist()]
        self.amplitudes = [[1.0]*n_sources]
                           
        return [self.coordinates,self.amplitudes]
    
    
if __name__ == '__main__':
    # Testing implementations
    import sys
    sys.path.append('/home/daep/w.goncalves/dev/beamforming/beamlib')
    import beamlib
    
    grid = beamlib.grid.Uniform(
        n=21,x=[-0.5,0.5],y=[-0.5,0.5],z=1.2)

    #for c in (SingleSource,TwoSources,TwinSources,Circular,RotatingLine,Line):
    for c in [ISAE]:
        sources = c()
        coordinates, amplitudes = sources.generate(grid=grid)
        print(coordinates)
        sources.plot(grid=grid)