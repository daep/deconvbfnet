#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Database generation script

Produces a database (*.npz file) with source map with the sources amplitudes
and classical beamforming map. It also exports the CSM arrays in order
to allow a compatison with other beamforming/deconvolution methods.

Antenna and source options are hardcoded here and on deconvnetlib.generator

----------
Flags:
    
    -n: number of cases. Default is 1000.
    
    -min: minimun number of sources. Default is 1.
    
    -max: maximun number of sources. Default is 10.
    
    -range: amplitudes range in decibels relative to the stronges source.
        All sources  have the same amplitude when range is null (default).
    
    -benchmarks: flag to indicate to generate the signals for benchmark cases,
        as defined in deconvnetlib.benchmarks. If used, `min`, `max` and
        `range` flags are ignored.
    
    -workers: number of processes called to generate the databse.
        Default is 5.
    
    -datafolder: string with the path where to save the database.
        Optional, default (None) will consider subfolder `database` of
        current working folder.
    
    -filename: string with the filename to be generated (without the file
        format). If None (default) a file with the frequency and number of
        cases will be used.
    
    -frequency: beamforming frequency in Hertz. Default is 2000.
    
    -size: scanning grid discretization. Default (21) will generate data
        for a 21 x 21 grid.

----------
Example:
    
    Command to produce a database file `data_test_3kHz.npz` with 100 cases,
    with 1 to 4 random sources of equal amplitude, at frequence 3 kHz,
    grid 10 x 10, on folder './database_small':
        
        ```
        python generate.py \
            -n=100 \
            -min=1 -max=4 \
            -datafolder='./database_small' \
            -filename='data_test_3kHz' \
            -frequency=3000 \
            -size=10
        ```
        
"""

import os
import argparse

import deconvnetlib


if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-n',default=1000,type=int)
    parser.add_argument('-min',default=1,type=int)
    parser.add_argument('-max',default=10,type=int)
    parser.add_argument('-range',default=0,type=int)
    parser.add_argument('-benchmarks',action='store_true')
    parser.add_argument('-workers',default=5,type=int)
    parser.add_argument('-datafolder',default=None,type=str)
    parser.add_argument('-filename',default=None,type=str)
    parser.add_argument('-frequency',default=2000)
    parser.add_argument('-size',default=21,type=int)
    
    args = parser.parse_args()
    
    data_folder = args.datafolder
    if data_folder is None:
        cwd = os.getcwd()
        data_folder = f'{cwd}/database/'
    os.makedirs(data_folder,exist_ok=True)
    
    frequency = int(args.frequency)
    workers = args.workers
    number_cases = args.n
    size = args.size
    
    filename = args.filename
    if filename is None:
        filename = f'data_f{frequency:05d}_n{number_cases:05d}'
    
    scan_grid_n = args.size
    # min to max number of sources
    range_number = [args.min, args.max]
    range_level = abs(args.range)
    
    generator = deconvnetlib.generator.Generator(
        database_folder=data_folder,typ='incoherent',
        grid=None,array=None,size=size,c0=343)
    
    if not args.benchmarks:
        generator.random(
            filename=filename,
            number_cases=number_cases,
            range_number=range_number,
            range_level=range_level,
            frequency=frequency,
            workers=workers)
    else:
        grid = generator.grid
        
        for distribution in (
                deconvnetlib.benchmarks.SingleSource(),
                deconvnetlib.benchmarks.TwoSources(),
                deconvnetlib.benchmarks.TwinSources(),
                deconvnetlib.benchmarks.Circular(),
                deconvnetlib.benchmarks.Line(),
                deconvnetlib.benchmarks.RotatingLine(),
                deconvnetlib.benchmarks.ISAE()):
            
            coordinates, amplitudes = distribution.generate(grid)
            generator.fixed(
                filename=f'benchmarks_{distribution.name}',
                coordinates=coordinates,
                amplitudes=amplitudes,
                frequency=frequency,
                workers=workers)
    